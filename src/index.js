const express = require('express');
const app = express();
const path =require('path');

app.set('port',8000);
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs')

//preprocesar antes de añadir a las rutas

//rutas
app.use(require('./routes/index'));

//archivos estaticos
app.use(express.static(path.join(__dirname,'public')));

//declarar el servidor
app.listen(app.get('port'), ()=>{
    console.log('sevidor',app.get('port'));
});