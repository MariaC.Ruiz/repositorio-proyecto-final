const express = require('express');
const router = express.Router();

router.get('/',(req, res) => {
    res.render('index.html' ,{title:'Localidad de Ciudad Bolivar'});
   
   });
   router.get('/contact',(req, res) => {
    res.render('contact.html' ,{title:'Agrega un evento'});
   });
   router.get('/lugares',(req, res) => {
    res.render('lugares.html' ,{title:'Visualiza el mapa de la localidad'});
   });
   router.get('/ubicacion',(req, res) => {
    res.render('/ubicacion.html' ,{title:'Visualiza el mapa de la localidad'});
   });
   router.get('/sitios',(req, res) => {
    res.render('sitios.html' ,{title:'Agrega un sitio de interes'});
   });
   module.exports= router;